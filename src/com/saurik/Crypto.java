package com.saurik;
import java.io.*;


class Crypto {
    private String rawText;
    private String path;
    private String fileName;
    private String fileDestinationName;
    private Boolean encrypted;
    private int aKey,bKey;
    private int m = 65536;
    private Object[] inputFile;
    Crypto(String inPath, String inFilename,String inFileDestinationName, int a, int b, boolean encrypt){
        path = inPath;
        fileDestinationName = inFileDestinationName;
        fileName = inFilename;
        encrypted = encrypt;
        aKey = a;
        bKey = b;
    }

    private String encryption(){
        StringBuilder result = new StringBuilder();
        for (int i = 0;i <= rawText.length() - 1;i++){
                result.append((char)((aKey * rawText.charAt(i) + bKey) % m));
            }
        return result.toString();
    }

    public void startWithFile(){
        try {
            FileInputStream fstream;
            if (encrypted) {
                fstream = new FileInputStream(path + fileName +".txt");
        }
            else{
                fstream = new FileInputStream(path + fileName +".txt");
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
            inputFile = br.lines().toArray();

            if (encrypted) {
                for (int i = 0; i <= inputFile.length - 1; i++) {
                    //System.out.println(inputFile[i]);
                    rawText = (String) inputFile[i];
                    EncryptionToFile();
                }

            } else {
                for (int i = 0; i <= inputFile.length - 1; i++) {
                    rawText = (String) inputFile[i];
                    DecryptionToFile();
                }
            }
        }
        catch (IOException e){
            System.out.println(e);
        }
    }

    private void EncryptionToFile(){
        try {
            FileWriter writer = new
                    FileWriter(path + fileDestinationName +".txt", true);
            writer.write(encryption() + '\n');
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void DecryptionToFile(){
        try {
            FileWriter writer = new
                    FileWriter(path + fileDestinationName +".txt", true);
            writer.write(decryption(rawText)+ '\n');
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    private String decryption(String input) {
        System.out.println(input);
        StringBuilder builder = new StringBuilder();
        int inverse = modInverse(aKey,m);
        //int inverse = BigInteger.valueOf(aKey).modInverse(BigInteger.valueOf(m));
        for (int in = 0; in < input.length(); in++) {
            char character = input.charAt(in);
                int decoded = inverse * (character - bKey + m);
                character = (char) (decoded % m);
            builder.append(character);
        }
        System.out.println(builder);
        return builder.toString();
    }


   int modInverse(int a, int n)
    {
        int i = n;
        int v = 0;
        int d = 1;
        while (a > 0)
        {
            int t = i / a, x = a;
            a = i % x;
            i = x;
            x = d;
            d = v - t * x;
            v = x;
        }
        v %= n;
        if (v < 0) v = (v + n) % n;
        return v;
    }
}
